#!/bin/sh
YUM_CMD=$(which yum)
APT_GET_CMD=$(which apt-get)
PACMAN_CMD=$(which apt-get)
OPENBSD_CMD=$(which pkg_add)





install() {
	if ! [ -x "$(command -v $1)" ]; then

		echo -n "Installing $1"
		
		if [[ ! -z $YUM_CMD ]]; then
     			yum install $1
 		elif [[ ! -z $APT_GET_CMD ]]; then
    			apt-get install $1
 		elif [[ ! -z $PACMAN_CMD ]]; then
 			pacman -S $1
		elif [[ ! -z $OPENBSD_CMD ]]; then
 			pacman -S $1
		else
    			echo "error can't install package $PACKAGE"
    			exit 1;
 		fi
	else
		echo "$1 already installed."
	fi
}


echo "Start? (y/n)"
answer=$( while ! head -c 1 | grep -i '[ny]' ;do true ;done )
if echo "$answer" | grep -iq "^y" ;then
	echo 
else
	echo "Oh!, ok then...."
	exit 0
fi

install pcmanfm
echo
#install taskwarrior
echo
install ranger
echo
install mc
echo
install vim
echo
install i3-gaps
echo
install calcurse
echo
install wordgrinder
echo
install htop
echo
install nitrogen
echo
install fish
echo
install curl
echo
install picom
echo
install dunst
echo
install blueman
echo 
install volumeicon
echo 
install network-manager-applet
echo
install rofi
echo
install pcmanfm
echo
install


curl -L https://get.oh-my.fish | fish


