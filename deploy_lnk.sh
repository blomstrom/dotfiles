#!/bin/sh

git clone https://github.com/morhetz/gruvbox.git ~/.vim/pack/default/start/gruvbox

mkdir -p ~/.config/i3
mkdir -p ~/.config/i3status
mkdir -p ~/.config/rofi



mv  ~/.config/i3/config ~/.config/i3/config.bak
mv  ~/.config/i3status/config ~/.config/i3status/config.bak
mv  ~/.config/compton.conf ~/.config/compton.conf.bak
mv  ~/.Xresources ~/.Xresources.bak
mv  ~/.vimrc ~/.vimrc.bak
mv  ~/.xinitrc ~/.xinitrc.bak
mv  ~/.config/rofi/config ~/config/rofi/config.bak





ln -s ~/dotfiles/i3/config ~/.config/i3/config
ln -s ~/dotfiles/i3/i3status.conf ~/.config/i3status/config
ln -s ~/dotfiles/compton.conf ~/.config/compton.conf
ln -s ~/dotfiles/Xresources ~/.Xresources
ln -s ~/dotfiles/vimrc ~/.vimrc
ln -s ~/dotfiles/xinitrc ~/.xinitrc
ln -s ~/dotfiles/rofi_config ~/.config/rofi/config


